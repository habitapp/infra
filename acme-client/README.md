# acme-client

Este contenedor se encarga de expedir certificados Let's Encrypt.

Los contenedores que necesiten acceso a las llaves privadas y
certificados tienen que montar los mismos directorios donde
`acme-client` les coloca.

Además, como `nginx` va a encargarse de recibir las peticiones de
verificación de LE, tiene que tener acceso al directorio
`/var/www/acme`, donde se almacenan los `challenges`.

## Agregar dominios

Para agregar dominios, hay que editar el archivo `domains` y agregar un
dominio por línea.  Los subdominios se escriben separados por espacios a
continuación del dominio principal.

```
$ cat domains
dominio.org sub.dominio.org smtp.dominio.org
otrodominio.tel otro.otrodominio.tel
```
