#!/bin/sh
set -e

install -dm 2750 -o postgres -g postgres /run/postgresql

# Salir inmediatamente si ya está configurada
if test ! -f ${PGDATA}/PG_VERSION ; then
  su - postgres -c "/usr/bin/initdb --locale ${LANG} -E UTF8 -D ${PGDATA}"
  su - postgres -c "/usr/bin/pg_ctl start --pgdata ${PGDATA}"
  su - postgres -c "/usr/bin/createuser --login ${PGUSER}"
  su - postgres -c "/usr/bin/createdb --owner ${PGUSER} ${PGDB}"
  su - postgres -c "/usr/bin/pg_ctl stop --pgdata ${PGDATA}"
  echo "host ${PGDB} ${PGUSER} samenet trust" >> ${PGDATA}/pg_hba.conf
  echo "listen_addresses = '*'" >> ${PGDATA}/postgresql.conf
fi

cmd="$@"

test "${cmd}" = "postgres" \
&& cmd='su-exec postgres postgres'

exec ${cmd}
