.SHELL = /bin/bash
.DEFAULT_GOAL := all

v ?= $(shell date +%F | tr "-" ".")
name := habitapp/$(d):$(v)

ifdef i
interactive := -it
extra ?= /bin/sh
endif

build:
	docker build -t $(name) $(d)/

tag:
	docker tag $(name) registry.forja.lainventoria.com.ar/$(name)
	docker tag $(name) registry.forja.lainventoria.com.ar/habitapp/$(d):latest

push:
	docker push registry.forja.lainventoria.com.ar/$(name)
	docker push registry.forja.lainventoria.com.ar/habitapp/$(d):latest

all: build tag push

help:
	@echo "make [all|build|tag|push] d=directorio [v=version] [i=true]"

run:
	docker run $(interactive) $(name) $(extra)
