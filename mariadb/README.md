# MariaDB

Inicia una base de datos MariaDB.  Coloca la contraseña de
administración en `/etc/mysql/alpine.cnf`.  Se crea un backup dentro de
`sql.d` para poder sobrevivir a reiniciadas.

Al iniciarse, corre los comandos de SQL que se ponen en el directorio
montado `sql.d`.

La cadena `PASSWORD` de cada archivo es reemplazada por una contraseña
aleatoria de 32 caracteres, que queda guardada en el archivo asociado.
Si las consultas que se ejecutan son idempotentes, no es necesario
eliminar los archivos y las contraseñas ya no se van a cambiar.

# TODO

* Marcar los `sql` como ya ejecutados
* Guardar las contraseñas en otro lado
