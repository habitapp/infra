create database if not exists matomo;
create user if not exists 'matomo' identified by 'PASSWORD';
grant all privileges on matomo.* to 'matomo';
flush privileges;
