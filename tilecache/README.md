Genera una caché estática de un mapa para usar con Leaflet.

# Adaptar

## mapnik.xml

Hoja de estilos y fuente de datos para Mapnik

## all\_tiles.py

El script que genera todas las `tiles`.  Modificar los parámetros de
`bounding box` (se pueden sacar de <https://tools.geofabrik.de/calc/>),
zoom mínimo y máximo.

# Correr

Compilar el contenedor con Docker y luego correr el script:

```bash
# Crear el contenedor
docker build -t tilecache .
# Iniciarlo y generar las tiles
docker run -it -v $PWD/tiles:/root/tiles/tiles tilecache python all_tiles.py
# Recuperar permisos
sudo chown -R ${USER}:${USER} tiles/
```

Las `tiles` van a quedar en el directorio `./tiles`.
